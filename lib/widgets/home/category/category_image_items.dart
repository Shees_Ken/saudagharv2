import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../models/category_model.dart';
import '../../../models/product_model.dart';

/// The category icon circle list
class CategoryItem extends StatelessWidget {
  final config;
  final products;
  final width;

  CategoryItem({this.config, this.products, this.width});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final itemWidth = (width ?? screenSize.width) / 3;
    final categoryList = Provider.of<CategoryModel>(context).categoryList;
    final id = config['category'].toString();
    final name = categoryList[id] != null ? categoryList[id].name : '';
    final image = categoryList[id] != null ? categoryList[id].image : '';
    final total = categoryList[id] != null ? categoryList[id].totalProduct : '';

    final imageWidget = config["image"] != null
        ? config["image"].toString().contains('http')
            ? Image.network(config["image"], fit: BoxFit.fitWidth)
            : Image.asset(
                config['image'],
                fit: BoxFit.fitWidth,
              )
        : null;

    return Padding(
      padding: const EdgeInsets.only(left: 5.0),
      child: GestureDetector(
        onTap: () => ProductModel.showList(config: config, context: context),
               child: Container(
//           decoration: BoxDecoration(
//              border: Border.all(
//                color: HexColor(
//                  "5F" + kNameToHex["grey"],
//                ),
//                width: 0.5,
//              ),
//             borderRadius: const BorderRadius.all(Radius.circular(5.0)),
//           ),
          margin: const EdgeInsets.only(left: 5, right: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  width: 40,
                  height: 40,
                  child: imageWidget != null
                      ? imageWidget
                      : Tools.image(
                          url: image,
                          fit: BoxFit.cover,
                          isResize: true,
                          size: kSize.small,
                        ),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0)),
                  ),
                ),
              ),
              if (config['showText'] == true) const SizedBox(height: 6),
//              if (config['showText'] == true)
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(top: 5, left: 0.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
//                        const SizedBox(height: 2),
                        Text(
                          config["name"],
                          style: const TextStyle(
                            fontSize: 9,

                            fontWeight: FontWeight.bold,
                          ),
                        ),
//                        const SizedBox(height: 4),
//                        Text(
//                          config["description"] ?? '$total products',
//                          style: const TextStyle(
//                            fontSize: 9,
//                          ),
//                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
 
      ),
    );
  }
}

/// List of Category Items
class CategoryImages extends StatelessWidget {
  final config;

  CategoryImages({this.config, Key key}) : super(key: key);

  List<Widget> listItem({width}) {
    List<Widget> items = [];
    for (var item in config['items']) {
      items.add(CategoryItem(
        config: item,
        width: width,
      ));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final itemWidth = constraints.maxWidth / 10;
        final heightList = itemWidth + 22;
        return config['wrap']
            ? Wrap(
                children: listItem(width: constraints.maxWidth),
                alignment: WrapAlignment.center,
              )
            : Container(
                height: 80,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: listItem(width: constraints.maxWidth),
                  ),
                ),
              );
      },
    );
  }
}
