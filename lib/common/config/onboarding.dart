// TODO: 4-Update Welcome Screens
/// the welcome screen data
/// set onBoardingData = [] if you would like to hide the onboarding
List onBoardingData = [
  {
    "title": "Welcome to SaudaGhar",
    "image": "assets/images/onboarding_1.png",
    "desc": "A One-Stop-Shop-All Solution To All Your Grocery Needs!",
    "background": "#FFF3F2"
  },
  {
    "title": "Fulfilling All Your Needs Under One Roof",
    "image": "assets/images/onboarding_2.png",
    "desc":
        "Whether you need veggies, staple, snacks, or everyday household items! We're providing all of that in just a few taps! Let's get started.",
    "background": "#F2FFFC"
  },
  {"image": "assets/images/onboarding_3.png", "background": "#F9F2FF"}
];
